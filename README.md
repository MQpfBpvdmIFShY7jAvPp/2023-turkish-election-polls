# 2023 Turkish Election Polls
Dependencies:


- [ggplot2](https://ggplot2.tidyverse.org/)
- [tidyr](https://tidyr.tidyverse.org/)

`install.packages(c("ggplot2", "tidyr"))`

The dates are the publication date for a given poll.
